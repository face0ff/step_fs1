package ua.danIt;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import ua.danIt.framework.ChoiseUserServlet;
import ua.danIt.framework.LoginServlet;
import ua.danIt.framework.SelectedUsersServlet;
import ua.danIt.framework.ChatRoomServlet;


public class App {

  public static void main(String[] args) throws Exception {

    Server server = new Server(8080);
    ServletContextHandler handler = new ServletContextHandler();

    ServletHolder holder = new ServletHolder(new ChoiseUserServlet());
    handler.addServlet(holder, "/*");


    LoginServlet loginServlet = new LoginServlet();
    handler.addServlet(new ServletHolder(loginServlet),"/login");

    SelectedUsersServlet selectedUsersServlet = new SelectedUsersServlet();
    handler.addServlet(new ServletHolder(selectedUsersServlet),"/message");

    ChatRoomServlet chatRoomServlet = new ChatRoomServlet();
    handler.addServlet(new ServletHolder(chatRoomServlet),"/message/*");

    server.setHandler(handler);
    server.start();
    server.join();

  }

}