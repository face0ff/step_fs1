package ua.danIt.Application.dao;

import java.sql.SQLException;
import java.util.List;

import ua.danIt.Application.model.User;

public interface UserDao {
  List<User> getAllUsers(int id);
  List<User> getSelectedUsers();
  void selectUser(Integer userId) throws SQLException;
   User getCurrentUser();
  User getUserByID(int id) throws SQLException;

}
