package ua.danIt.Application.model;

public class User {
  public String NAME;
  public int ID;
  public String URL;
  public  String EMAIL;
  public  String LOGIN;
  public  String GENDER;
  public  String PASSWORD;


  public User(String NAME, int ID, String URL) {
    this.NAME = NAME;
    this.ID = ID;
    this.URL = URL;
  }
  public  User(){

  }

  public User(int ID, String NAME, String EMAIL, String GENDER, String LOGIN, String PASSWORD) {
    this.NAME = NAME;
    this.ID = ID;
    this.EMAIL = EMAIL;
    this.LOGIN = LOGIN;
    this.GENDER = GENDER;
    this.PASSWORD = PASSWORD;
  }

  public User(String NAME, int ID) {
    this.NAME = NAME;
    this.ID = ID;

  }


  public String getNAME() {
    return NAME;
  }

  public int getID() {
    return ID;
  }

  public String getURL() {
    return URL;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof User))
      return false;

    User user = (User) o;

    if (getID() != user.getID())
      return false;
    if (getNAME() != null ? !getNAME().equals(user.getNAME()) : user.getNAME() != null)
      return false;
    if (getURL() != null ? !getURL().equals(user.getURL()) : user.getURL() != null)
      return false;
    if (EMAIL != null ? !EMAIL.equals(user.EMAIL) : user.EMAIL != null)
      return false;
    if (LOGIN != null ? !LOGIN.equals(user.LOGIN) : user.LOGIN != null)
      return false;
    if (GENDER != null ? !GENDER.equals(user.GENDER) : user.GENDER != null)
      return false;
    return PASSWORD != null ? PASSWORD.equals(user.PASSWORD) : user.PASSWORD == null;
  }

  @Override
  public int hashCode() {
    int result = getNAME() != null ? getNAME().hashCode() : 0;
    result = 31 * result + getID();
    result = 31 * result + (getURL() != null ? getURL().hashCode() : 0);
    result = 31 * result + (EMAIL != null ? EMAIL.hashCode() : 0);
    result = 31 * result + (LOGIN != null ? LOGIN.hashCode() : 0);
    result = 31 * result + (GENDER != null ? GENDER.hashCode() : 0);
    result = 31 * result + (PASSWORD != null ? PASSWORD.hashCode() : 0);
    return result;
  }
}
