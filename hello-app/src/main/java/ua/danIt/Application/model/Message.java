package ua.danIt.Application.model;

public class Message {
  public int ID;
  public int FROM_USER;
  public int TO_USER;
  public String MSG_TEXT;
  public Long TIMESTAMP;

  public Message(int FROM_USER, int TO_USER, String MSG_TEXT) {
    this.FROM_USER = FROM_USER;
    this.TO_USER = TO_USER;
    this.MSG_TEXT = MSG_TEXT;
  }
  public Message(int FROM_USER, int TO_USER, String MSG_TEXT, long TIMESTAMP){
    this.FROM_USER = FROM_USER;
    this.TO_USER = TO_USER;
    this.MSG_TEXT = MSG_TEXT;
    this.TIMESTAMP = TIMESTAMP;
  }


  public Message(){

  }

  public int getID() {
    return ID;
  }

  public void setID(int ID) {
    this.ID = ID;
  }

  public int getFROM_USER() {
    return FROM_USER;
  }

  public void setFROM_USER(int FROM_USER) {
    this.FROM_USER = FROM_USER;
  }

  public int getTO_USER() {
    return TO_USER;
  }

  public void setTO_USER(int TO_USER) {
    this.TO_USER = TO_USER;
  }

  public String getMSG_TEXT() {
    return MSG_TEXT;
  }

  public void setMSG_TEXT(String MSG_TEXT) {
    this.MSG_TEXT = MSG_TEXT;
  }
}
