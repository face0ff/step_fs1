package ua.danIt.framework.utils;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.google.common.collect.Lists;

public class EntityUtils {

  public static  <T> List<T> nativeQuery(String sql, Class<T> clazz) {
    List<T> result = Lists.newArrayList();
    String PASSWD = "paRis";
    String DB_URL = "jdbc:mysql://danit.cukm9c6zpjo8.us-west-2.rds.amazonaws.com:3306/fs1";
    String USER = "fs1_user";
    try {
      Connection con = DriverManager.getConnection(DB_URL, USER, PASSWD);
      Statement statement = con.createStatement();
      ResultSet resultSet = statement.executeQuery(sql);
      ResultSetMetaData metaData = resultSet.getMetaData();

      while (resultSet.next()) {

        T instance = getInstance(clazz, resultSet, metaData);
        result.add(instance);
      }

      resultSet.close();
      statement.close();
      con.close();


    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return result;
  }

  public static  <T> T getInstance(Class<T> clazz, ResultSet resultSet, ResultSetMetaData metaData) throws InstantiationException, IllegalAccessException, SQLException, NoSuchFieldException {
    T instance = clazz.newInstance();
    int columnCount = metaData.getColumnCount();

    for (int i = 1; i <= columnCount; i++) {

      String columnLabel = metaData.getColumnLabel(i);
      Field field = clazz.getDeclaredField(columnLabel);
      Object values = resultSet.getObject(i, field.getType());

      field.setAccessible(true);
      field.set(instance, values);
    }

    return instance;
  }
}
