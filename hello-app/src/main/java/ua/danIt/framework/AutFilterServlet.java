package ua.danIt.framework;

import java.io.IOException;
import java.util.logging.Filter;
import java.util.logging.LogRecord;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AutFilterServlet implements Filter {

    private ServletContext context;

    public void init(FilterConfig fConfig) throws ServletException {
      this.context = fConfig.getServletContext();
      this.context.log("AuthenticationFilter initialized");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

      HttpServletRequest req = (HttpServletRequest) request;
      HttpServletResponse res = (HttpServletResponse) response;

      String uri = req.getRequestURI();
      this.context.log("Requested Resource::"+uri);

      HttpSession session = req.getSession(false);

      if(session == null && !(uri.endsWith("html") || uri.endsWith("LoginServlet"))){
        this.context.log("Unauthorized access request");
        res.sendRedirect("/login");
      }else{
        // pass the request along the filter chain
        chain.doFilter(request, response);
      }


    }

  @Override
  public boolean isLoggable(LogRecord logRecord) {
    return false;
  }
}